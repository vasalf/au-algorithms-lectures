Чтобы собрать конспект, надо запустить

```bash
xelatex -shell-escape -interaction=nonstopmode conspect.tex
```

Я реально не смотрел, какие TeX-пакеты нужны, чтобы это собиралось, но гарантирую, что в репозиториях Debian всё есть.